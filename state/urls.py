from django.urls import path
from state.views import list_states, list_cities, to_do_list, team_view

urlpatterns = [
    path("list/", list_states, name="list_states"),
    path("city/<int:id>/", list_cities, name="list_cities"),
    path("city/<int:id>/todo", to_do_list, name="to_do_list"),
    path("team/", team_view, name="team")
]

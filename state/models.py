from django.db import models

# Create your models here.


class State(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField(max_length=200)

    def __str__(self):
        return self.title


class City(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField()
    picture = models.URLField(max_length=200)
    state = models.ForeignKey(
        State,
        related_name="states",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title

class ThingsToDo(models.Model):
    name = models.CharField(max_length=150)
    activity1 = models.CharField(max_length=50)
    activity2 = models.CharField(max_length=50)
    activity3 = models.CharField(max_length=50)
    activity4 = models.CharField(max_length=50)
    city = models.ForeignKey(
        City,
        related_name="cities",
        on_delete=models.CASCADE,
        )

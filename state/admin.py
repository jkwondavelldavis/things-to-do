from django.contrib import admin
from state.models import State, City, ThingsToDo

# Register your models here.


@admin.register(State)
class State(admin.ModelAdmin):
    list_display = [
        "title",
        "picture",
        "id",
    ]


@admin.register(City)
class City(admin.ModelAdmin):
    list_display = [
        "title",
        "description",
        "picture",
        "id",
    ]

@admin.register(ThingsToDo)
class ThingsToDo(admin.ModelAdmin):
    list_display = [
        "name",
        "activity1",
        "activity2",
        "activity3",
        "activity4",
        "city",
    ]

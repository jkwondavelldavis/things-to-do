from django.shortcuts import render
from state.models import State, City
from django.contrib.auth.decorators import login_required
# Create your views here.


@login_required
def list_states(request):
    lists = State.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "places/states.html", context)


@login_required
def list_cities(request, id):
    cities = State.objects.get(id=id)
    context = {
        "cities": cities,
    }
    return render(request, "places/cities.html", context)

@login_required
def to_do_list(request, id):
    activity = City.objects.get(id=id)
    context = {
        "activity": activity,
    }
    return render(request, "places/todo.html", context)


@login_required
def team_view(request):
    return render(request, "places/team.html")

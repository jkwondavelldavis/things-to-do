from django.urls import path
from .views import user_login, signup, logout_user


urlpatterns = [
    path("login/", user_login, name="login"),
    path("signup/", signup, name="signup"),
    path("logout/", logout_user, name="logout"),
]
